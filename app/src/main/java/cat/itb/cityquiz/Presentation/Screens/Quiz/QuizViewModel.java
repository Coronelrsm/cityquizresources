package cat.itb.cityquiz.Presentation.Screens.Quiz;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {
    // TODO: Implement the ViewModel

    GameLogic gameLogic;
    MutableLiveData<Game> game;

    public void startGame(){
        gameLogic = RepositoriesFactory.getGameLogic();
        game = new MutableLiveData<>(gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers)); //Classe.constant

    }


    public MutableLiveData<Game> getGame() {
        return game;
    }

    public int getScore(){
        return game.getNumCorrectAnswers(); //abans tenia un game.getScore, pero NO era el metode correcte
    }

    public void checkAnswer(int answer) {
        game = gameLogic.answerQuestions(game, answer);


    }
}

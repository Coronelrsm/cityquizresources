package cat.itb.cityquiz.Presentation.Screens.Quiz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.Presentation.Screens.Start.StartFragmentDirections;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;

public class QuizFragment extends Fragment implements View.OnClickListener {

    ImageView cityImageQuiz;
    MaterialButton option1;
    MaterialButton option2;
    MaterialButton option3;
    MaterialButton option4;
    MaterialButton option5;
    MaterialButton option6;

    private QuizViewModel mViewModel;
    View globalView;


    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quiz_fragment, container, false);



        //per fer butterknife, fer alt insert a quiz_fragment
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        globalView = view;
        cityImageQuiz = view.findViewById(R.id.cityImageQuiz);
        option1 = view.findViewById(R.id.option1);
        option1.setOnClickListener(this); //No navegava per les opcions degut a que no tenia implementat el onClick, que es fa amb boto.setOnClickListener
        option2 = view.findViewById(R.id.option2);
        option2.setOnClickListener(this);
        option3 = view.findViewById(R.id.option3);
        option3.setOnClickListener(this);
        option4 = view.findViewById(R.id.option4);
        option4.setOnClickListener(this);
        option5 = view.findViewById(R.id.option5);
        option5.setOnClickListener(this);
        option6 = view.findViewById(R.id.option6);
        option6.setOnClickListener(this);




    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*
        Per tal que tots els fragments puguin accedir al mateix Game, farem que comparteixin el mateix viewModel.
        Per fer això, al fer la crida al ViewModelProvider li hem de passar l'Activity i no el Fragment.
         */
        //quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        mViewModel.startGame(); //crea el joc
        mViewModel.getGame().observe(this, this::display); //enlloc de tenir Game game = algo, tenim viewModel que fa getGame().observe(fragment, display)



    }

    private void display(Game game) {
    if (game.isFinished()) {  //donava null a answers quan el joc s'havia acabat
        //implementar la navegació a resultat

        NavDirections action = QuizFragmentDirections.navigateToResultFragment();
        Navigation.findNavController(globalView).navigate(action);
    }

    else {
        List<City> answers = game.getCurrentQuestion().getPossibleCities();

        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        cityImageQuiz.setImageResource(resId);

        //les opcions, com que són una list, les imprimim amb el text segons la posició
        option1.setText(answers.get(0).getName());
        option2.setText(answers.get(1).getName());
        option3.setText(answers.get(2).getName());
        option4.setText(answers.get(3).getName());
        option5.setText(answers.get(4).getName());
        option6.setText(answers.get(5).getName());
    }

    }






    @OnClick ({R.id.option1, R.id.option2, R.id.option3, R.id.option4, R.id.option5, R.id.option6})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.option1:
                //comparem el nom del botó amb el nom de la resposta correcta
                questionAnswered(0);
                break;
            case R.id.option2:
                questionAnswered(1);
                break;
            case R.id.option3:
                questionAnswered(2);
                break;
            case R.id.option4:
                questionAnswered(3);
                break;
            case R.id.option5:
                questionAnswered(4);
                break;
            case R.id.option6:
                questionAnswered(5);
                break;

        }
    }

    private void questionAnswered(int answer) {
        mViewModel.checkAnswer(answer);
        mViewModel.getGame().observe(this, this::display); //enlloc de tenir Game game = algo, tenim viewModel que fa getGame().observe(fragment, display)

        //game.questionAnswered(mViewModel.game.getCurrentQuestion().getCorrectCity());
        //if (!mViewModel.game.isFinished()) {
          //  display();
        //}

    }


    }


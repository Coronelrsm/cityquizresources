package cat.itb.cityquiz.Presentation.Screens.Start;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import cat.itb.cityquiz.Presentation.Screens.Quiz.QuizViewModel;
import cat.itb.cityquiz.R;

public class StartFragment extends Fragment {

    private QuizViewModel mViewModel;
    TextView welcomeToTheQuiz;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }


    //escrivint onvi i fent clic n'hi ha prou perquè es crei el mètode
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button startQuizButton = view.findViewById(R.id.startButton);
        welcomeToTheQuiz = view.findViewById(R.id.welcome);

        startQuizButton.setOnClickListener(this::startQuiz); //per fer navegació amb botons fem this::metode. I fem alt Enter i el crea



    }

    private void startQuiz(View view) {
        NavDirections action = StartFragmentDirections.navigationGoToQuizFragment();
        Navigation.findNavController(view).navigate(action);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        // TODO: Use the ViewModel
    }

}

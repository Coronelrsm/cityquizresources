package cat.itb.cityquiz.Presentation.Screens.Result;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import cat.itb.cityquiz.Presentation.Screens.Quiz.QuizViewModel;
import cat.itb.cityquiz.R;

public class ResultFragment extends Fragment {

    private QuizViewModel mViewModel;
    TextView score;
    Button playAgain;

    public static ResultFragment newInstance() {
        return new ResultFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.result_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        score = view.findViewById(R.id.score);
        playAgain = view.findViewById(R.id.playAgain);
        playAgain.setOnClickListener(this::replay);


    }

    private void replay(View view) { //per fragment directions es fa rebuild project després de crear la navegació.
        NavDirections action = ResultFragmentDirections.navigateRestartGame();
        Navigation.findNavController(view).navigate(action);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);

        String puntuacio = String.valueOf(mViewModel.getScore());
        score.setText(puntuacio); //aquí abans petava perquè directament feia un setText(mviewmodel.getScore())

        String tornarAJugar = playAgain.getText().toString().trim();
        playAgain.setText(tornarAJugar);
    }

}
